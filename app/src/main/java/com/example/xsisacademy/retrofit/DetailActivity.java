package com.example.xsisacademy.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.xsisacademy.retrofit.model.PlayerData;

public class DetailActivity extends AppCompatActivity {

    ImageView detailPhoto;
    TextView detailName, detailCountry, detailCity;

    public static final String EXTRA_DETAIL = "extra_detail";

    private PlayerData playerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        playerData = (PlayerData) getIntent().getSerializableExtra(EXTRA_DETAIL);
        setupView();
        populateView(playerData);
    }

    private void populateView(PlayerData data) {
        Glide.with(this)
                .load(data.getImgUrl())
                .into(detailPhoto);
        detailName.setText(data.getName());
        detailCountry.setText(data.getCountry());
        detailCity.setText(data.getCity());
    }

    private void setupView() {
        detailPhoto = findViewById(R.id.imgDetail);
        detailName = findViewById(R.id.detailName);
        detailCountry = findViewById(R.id.detailCountry);
        detailCity = findViewById(R.id.detailCity);
    }
}
