package com.example.xsisacademy.retrofit.adapter;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.xsisacademy.retrofit.DetailActivity;
import com.example.xsisacademy.retrofit.R;
import com.example.xsisacademy.retrofit.model.PlayerData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XsisAcademy on 25/06/2019.
 */

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.CardViewHolder> {

    private Context context;
    private List<PlayerData> list;

    public CardViewAdapter(Context context, List<PlayerData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(context).inflate(R.layout.layout_cardview, parent, false);
        return new CardViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        final PlayerData data = list.get(position);
        Glide.with(context)
                .load(data.getImgUrl())
                .into(holder.imgPhoto);
        holder.itemNum.setText(data.getId());
        holder.itemName.setText(data.getName());
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailActivity.class);
                intentDetail.putExtra(DetailActivity.EXTRA_DETAIL, data);
                context.startActivity(intentDetail);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView itemNum, itemName;
        CardView cardViewHolder;
        Button btnDetail;

        public CardViewHolder(View itemView) {
            super(itemView);
            cardViewHolder = itemView.findViewById(R.id.cardViewHolder);
            imgPhoto = itemView.findViewById(R.id.imgPhoto);
            itemNum = itemView.findViewById(R.id.itemNum);
            itemName = itemView.findViewById(R.id.itemName);
            btnDetail = itemView.findViewById(R.id.btnDetail);
        }
    }
}
