package com.example.xsisacademy.retrofit;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xsisacademy.retrofit.adapter.CardViewAdapter;
import com.example.xsisacademy.retrofit.model.DataModel;
import com.example.xsisacademy.retrofit.model.PlayerData;
import com.example.xsisacademy.retrofit.network.GetDataService;
import com.example.xsisacademy.retrofit.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView itemCategory;
    private GetDataService service;
    DataModel dataModel;
    ProgressDialog progressDialog;
    private List<PlayerData> list;
    private CardViewAdapter cardViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
        networkRequest();
    }

    private void setupView(){
        itemCategory = findViewById(R.id.itemCategory);
    }

    private void networkRequest() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        service = RetrofitClient.getRetrofitInstance().create(GetDataService.class);
        service.getData().enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                progressDialog.dismiss();

                if (response.body() != null) {
                    dataModel = response.body();
                    list = dataModel.getData();
                    showRecyclerView(list);
                }


            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Connection Problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showRecyclerView(List<PlayerData> list) {
        itemCategory.setLayoutManager(new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
        ));
        cardViewAdapter = new CardViewAdapter(this, list);
        itemCategory.setAdapter(cardViewAdapter);
    }


}
