package com.example.xsisacademy.retrofit.network;

import com.example.xsisacademy.retrofit.model.DataModel;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by XsisAcademy on 24/06/2019.
 */

public interface GetDataService {
    @GET("json_parsing.php")
    Call<DataModel> getData();
}
